function proba_nombres(borne, taille_echantillons) {
	let resultat= {}
		
	for (let valPossible= 0; valPossible < borne; valPossible++) {
			resultat[valPossible]= 0	
	}
	
	for (let tirage= 0; tirage < taille_echantillons; tirage++){
		const nb= Math.floor(Math.random() * borne)
		resultat[nb]++;		
	}
	console.log(resultat)

	//let element= {'min': 0, 'max': 0}

	let minValue= resultat[0];
	let maxValue= resultat[0]
	for (let i= 1; i < borne; i++) {
		valeurTraite= resultat[i];
		if (valeurTraite < minValue){
			minValue= valeurTraite;
			//element['min']= minValue;
		}
		if (valeurTraite > maxValue){
			maxValue= valeurTraite;
			//element['max']= maxValue;
		}
	}
	let arrayResultat= {min: minValue, max: maxValue}
	return arrayResultat
	
}
console.log(proba_nombres(5, 70))




function calcul_ecart(borne, taille, nombreEssais) {

	let ecartFreq= {'ecartMin': 0, 'ecartMax': 0, 'somme': 0}

	for (let nb= 0; nb < nombreEssais; nb++){
		const essai= proba_nombres(borne, taille);
		ecartFreq.ecartMin = essai.min
		ecartFreq.ecartMax = essai.max
		const ecart= essai.max - essai.min;
		ecartFreq.somme += ecart;
	
	}
	ecartFreq.moyenne= ecartFreq.somme / nombreEssais
	return ecartFreq
}

console.log(calcul_ecart(5,70,4))


function pourcentage_echantillons(borne, taille) {
	
	for (const element of taille){
		let resultat = proba_nombres(borne, element)
		let freqMin = (resultat["min"]/element) * 100
		let freqMax = (resultat["max"]/element) * 100
		console.log("tailles: ", element, "frequence min %: ", freqMin, "frequence max %: ", freqMax)
	}
}

console.log(pourcentage_echantillons(5, [10,20,30,40]))